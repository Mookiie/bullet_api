<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MS_site extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Functions');
    $this->now = $this->Functions->date_time_get();

  }

  private function GenerateID()
  {
    $sql = "SELECT site_id FROM ms_site ORDER BY site_id DESC LIMIT 1";
    $qry = $this->db->query($sql);
        if($qry->num_rows()>0){
          $data = $qry->result_array();
          $oldId = $data[0]["site_id"];
          $oldYMD =  substr($oldId,1,6);
          $y = sprintf("%02d",date('y'));
          $m = sprintf("%02d",date('m'));
          $d = sprintf("%02d",date('d'));
          $newYMD =  $y.$m.$d;
          $oldNUM =  sprintf("%03d",substr($oldId,7,6));
          if ($oldYMD == $newYMD) {
              $newNUM = $oldNUM+1;
              $newNUM = sprintf("%03d",$newNUM);
          }else{
              $newNUM = "001";
          }
          return "S".$newYMD.$newNUM;
        }else{
          $y = sprintf("%02d",date('y'));
          $m = sprintf("%02d",date('m'));
          $d = sprintf("%02d",date('d'));
          $newYMD =  $y.$m.$d;
          $newNUM = "001";
          return "S".$newYMD.$newNUM;
        }
  }

  public function create($site_name,$site_description,$dep_id)
  {
    $callback = array();
    $Id = $this->GenerateID();
    $sql_insert = "INSERT INTO ms_site
                               VALUES('$Id','$site_name','$site_description','1','$dep_id')";
    $qry  = $this->db->query($sql_insert);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                      "id" => $Id
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function edit($Id,$site_name,$site_description,$dep_id)
  {
    $callback = array();
    $oldData = $this->getDataById($Id);
    $sql = "UPDATE ms_site
            SET  site_name = '$site_name',site_description = '$site_description',dep_id = '$dep_id'
            WHERE site_id = '$Id'";
    $qry  = $this->db->query($sql);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                      "data" => $oldData[0]
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function change_status($Id,$status)
  {
    $callback = array();
    $sql = "UPDATE ms_site
            SET site_status = '$status'
            WHERE site_id = '$Id'";
    $qry  = $this->db->query($sql);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function getDataByRoot($dep_id)
  {
    $sql = "SELECT * FROM ms_site WHERE dep_id = '$dep_id'";
    $qry = $this->db->query($sql);
    return $qry->result_array();
  }

  public function getDataById($Id)
  {
    $sql = "SELECT * FROM ms_site WHERE site_id = '$Id'";
    $qry = $this->db->query($sql);
    return $qry->result_array();
  }
}
