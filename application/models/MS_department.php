<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MS_department extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Functions');
    $this->now = $this->Functions->date_time_get();

  }

  private function GenerateID()
  {
    $sql = "SELECT dep_id FROM ms_department ORDER BY dep_id DESC LIMIT 1";
    $qry = $this->db->query($sql);
        if($qry->num_rows()>0){
          $data = $qry->result_array();
          $oldId = $data[0]["dep_id"];
          $oldYMD =  substr($oldId,1,6);
          $y = sprintf("%02d",date('y'));
          $m = sprintf("%02d",date('m'));
          $d = sprintf("%02d",date('d'));
          $newYMD =  $y.$m.$d;
          $oldNUM =  sprintf("%03d",substr($oldId,7,6));
          if ($oldYMD == $newYMD) {
              $newNUM = $oldNUM+1;
              $newNUM = sprintf("%03d",$newNUM);
          }else{
              $newNUM = "001";
          }
          return "D".$newYMD.$newNUM;
        }else{
          $y = sprintf("%02d",date('y'));
          $m = sprintf("%02d",date('m'));
          $d = sprintf("%02d",date('d'));
          $newYMD =  $y.$m.$d;
          $newNUM = "001";
          return "D".$newYMD.$newNUM;
        }
  }

  public function create($dep_name,$dep_caption,$branch_id)
  {
    $callback = array();
    $Id = $this->GenerateID();
    $sql_insert = "INSERT INTO ms_department
                               VALUES('$Id','$dep_name','$dep_caption','1','$branch_id')";
    $qry  = $this->db->query($sql_insert);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                      "id" => $Id
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function edit($Id,$dep_name,$dep_caption,$branch_id)
  {
    $callback = array();
    $oldData = $this->getDataById($Id);
    $sql = "UPDATE ms_department
            SET  dep_name = '$dep_name',dep_caption = '$dep_caption',branch_id = '$branch_id'
            WHERE dep_id = '$Id'";
    $qry  = $this->db->query($sql);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                      "data" => $oldData[0]
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function change_status($Id,$status)
  {
    $callback = array();
    $sql = "UPDATE ms_department
            SET dep_status = '$status'
            WHERE dep_id = '$Id'";
    $qry  = $this->db->query($sql);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function getDataByRoot($branch_id)
  {
    $sql = "SELECT * FROM ms_department WHERE branch_id = '$branch_id'";
    $qry = $this->db->query($sql);
    return $qry->result_array();
  }

  public function getDataById($Id)
  {
    $sql = "SELECT * FROM ms_department WHERE dep_id = '$Id'";
    $qry = $this->db->query($sql);
    return $qry->result_array();
  }



}
