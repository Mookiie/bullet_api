<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MS_team extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Functions');
    $this->now = $this->Functions->date_time_get();
  }

  private function GenerateID()
  {
    $sql = "SELECT team_id FROM ms_team ORDER BY team_id DESC LIMIT 1";
    $qry = $this->db->query($sql);
        if($qry->num_rows()>0){
          $data = $qry->result_array();
          $oldId = $data[0]["team_id"];
          $oldYMD =  substr($oldId,1,6);
          $y = sprintf("%02d",date('y'));
          $m = sprintf("%02d",date('m'));
          $d = sprintf("%02d",date('d'));
          $newYMD =  $y.$m.$d;
          $oldNUM =  sprintf("%03d",substr($oldId,7,6));
          if ($oldYMD == $newYMD) {
              $newNUM = $oldNUM+1;
              $newNUM = sprintf("%03d",$newNUM);
          }else{
              $newNUM = "001";
          }
          return "T".$newYMD.$newNUM;
        }else{
          $y = sprintf("%02d",date('y'));
          $m = sprintf("%02d",date('m'));
          $d = sprintf("%02d",date('d'));
          $newYMD =  $y.$m.$d;
          $newNUM = "001";
          return "T".$newYMD.$newNUM;
        }
  }

  public function create($team_name,$site_id)
  {
    $callback = array();
    $Id = $this->GenerateID();
    $sql_insert = "INSERT INTO ms_team
                               VALUES('$Id','$team_name','1','$site_id')";
    $qry  = $this->db->query($sql_insert);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                      "id" => $Id
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function edit($Id,$tean_name,$site_id)
  {
    $callback = array();
    $oldData = $this->getDataById($Id);
    $sql = "UPDATE ms_team
            SET  team_name = '$tean_name', site_id = '$site_id'
            WHERE site_id = '$Id'";
    $qry  = $this->db->query($sql);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                      "data" => $oldData[0]
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function change_status($Id,$status)
  {
    $callback = array();
    $sql = "UPDATE ms_team
            SET team_status = '$status'
            WHERE team_id = '$Id'";
    $qry  = $this->db->query($sql);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function getDataByRoot($site_id)
  {
    $sql = "SELECT * FROM ms_team WHERE site_id = '$site_id'";
    $qry = $this->db->query($sql);
    return $qry->result_array();
  }

  public function getDataById($Id)
  {
    $sql = "SELECT * FROM ms_team WHERE team_id = '$Id'";
    $qry = $this->db->query($sql);
    return $qry->result_array();
  }

  public function getDataByCompanyId($site_id)
  {
    if ($site_id != "") {
    $site_id = "AND site_id in ('$site_id')";
    }
    $sql = "SELECT * FROM ms_team WHERE team_status = '1' $site_id";
    $qry = $this->db->query($sql);
    if ($qry) {
      if ($qry->num_rows()>0) {
        $rowteam = $qry->result_array();
        $callback = array(
                            "status" => 200,
                            "type" => TRUE,
                            "msg" => "OK",
                            "data" => $rowteam
                          );
      }else {
        $callback = array(
                            "status" => 404,
                            "type" => FALSE,
                            "msg" => "Not Found",
                            "data" =>$sql
                          );
      }
    }else {
      $callback = array(
                          "status" => 405,
                          "type" => FALSE,
                          "msg" => "Query Error",
                          "data" =>$sql
                        );
    }
    return $callback;
  }

}
