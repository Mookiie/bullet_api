<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Functions');
    $this->now = $this->Functions->date_time_get();
    $this->path = $this->Functions->get_path();
  }

  private function GenerateUID()
  {
    $query = "SELECT uid FROM users ORDER BY uid DESC LIMIT 1";
    $qryid = $this->db->query($query);
    if($qryid->num_rows()>0){
      $userId = $qryid->result_array();
      $oldId = $userId[0]["uid"];
      $oldYMD =  substr($oldId,1,6);
      $y = sprintf("%02d",date('y'));
      $m = sprintf("%02d",date('m'));
      $d = sprintf("%02d",date('d'));
      $newYMD =  $y.$m.$d;
      $oldNUM =  sprintf("%06d",substr($oldId,7,6));
      if ($oldYMD == $newYMD) {
          $newNUM = $oldNUM+1;
          $newNUM = sprintf("%06d",$newNUM);
      }else{
          $newNUM = "000001";
      }
      return "U".$newYMD.$newNUM;
    }else{
      $y = sprintf("%02d",date('y'));
      $m = sprintf("%02d",date('m'));
      $d = sprintf("%02d",date('d'));
      $newYMD =  $y.$m.$d;
      $newNUM = "000001";
      return "U".$newYMD.$newNUM;
    }
  }

  public function createuser($username,$email,$password,$fname,$lname,$gender,$company_id,$branch_id,$dep_id,$site_id,$team_id,$level_id,$create_by,$operator)
  {
    $callback = array();
    $Id = $this->GenerateUID();
    $code = base64_encode(base64_encode(base64_encode($this->Functions->randomString(10))));
    $sql_insert = "INSERT INTO users
                   VALUES('$Id','$username','$email','$password','$fname','$lname',
                          '$gender','user.png','$company_id','$branch_id','$dep_id','$site_id','$team_id','$level_id',
                          '$create_by','$this->now',null,null,'0','$operator','9','$code')";
    $qry  = $this->db->query($sql_insert);
    if($qry){
       $this->Users_operator->createoperator($uid,$company_id,$branch_id,$dep_id,$site_id,$team_id,$create_by,$level_id);
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                      "id" => $Id
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function changePassword($uid,$password)
  {
    $callback = chk_checkpassword($uid,$password);
    if ($callback['status'] == 200) {
      $sql = "UPDATE users SET password = '$password' WHERE uid = '$uid'";
      $qry  = $this->db->query($sql);
      if($qry){
                      $callback = array(
                        "status" => 200,
                        "type" => TRUE,
                        "msg" => "OK",
                      );
       }else{
                        $callback = array(
                          "status" => 400,
                          "type" => FALSE,
                          "msg" => "Insert Failed",
                        );
      }
      return $callback;
    }
    else {
      return $callback;
    }
  }

  public function updateoperator($uid,$operator)
  {
    $sql = "UPDATE users
            SET operator = '$operator'
            WHERE uid = '$uid'";
    $qry  = $this->db->query($sql);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function getUserById($Id)
  {
  	$sql = "SELECT * FROM users WHERE uid = '$Id'";
  	$qry = $this->db->query($sql);
	   if ($qry->num_rows() > 0) {
  		$callback = array(
                          "status" => 200,
                          "type" => TRUE,
            						  "msg" => "OK",
            						  "data" => $qry->result_array()[0]
            						 );
  	}else{
  		$callback = array(
                          "status" => 404,
                          "type" => FALSE,
                          "msg" => "Not Found",
                          "data" => ""
                        );
  	}
  	return $callback;
  }

  public function getUserByUsername($username)
  {
  	$sql = "SELECT * FROM users WHERE username = '$username'";
  	$qry = $this->db->query($sql);
  	if ($qry->num_rows() > 0) {
  		$callback = array(
                          "status" => 200,
                          "type" => TRUE,
  						  "msg" => "OK",
  						  "data" => $qry->result_array()[0]
  						 );
  	}else{
  		$callback = array(
                          "status" => 404,
                          "type" => FALSE,
                          "msg" => "Not Found",
                          "data" => ""
                        );
  	}
  	return $callback;
  }

  public function getUserByEmail($email)
  {
  	$sql = "SELECT * FROM users WHERE email = '$email'";
  	$qry = $this->db->query($sql);
  	if ($qry->num_rows() > 0) {
    		$callback = array(
                            "status" => 200,
                            "type" => TRUE,
              						  "msg" => "OK",
              						  "data" => $qry->result_array()
              						 );
    	}else{
    		$callback = array(
                            "status" => 404,
                            "type" => FALSE,
                            "msg" => "Not Found",
                            "data" => ""
                          );
    }
  	return $callback;
  }

  public function getProfile($Id)
  {
    $sql = "SELECT uid, username, email, fname, lname, gender, image, company_id, branch_id, dep_id, site_id, team_id, level_id, create_by, create_date, update_by, update_date, admin, operator, user_status, code FROM users WHERE uid = '$Id'";
  	$qry = $this->db->query($sql);
	   if ($qry->num_rows() > 0) {
      $rowuser = $qry->result_array()[0];
      $sql_level = "SELECT * FROM db_service.users_view WHERE uid = '$Id' ORDER BY level_id ASC limit 1";
      $qry_level = $this->db->query($sql_level)->result_array()[0]['level_id'];
      $level = $this->MS_levels->getDataById($qry_level);
      $rowuser['level'] = $level;
      if ($rowuser["operator"] >= 1) {
        $rowuser['user_oper'] = $this->Users_operator->getOperation($rowuser["uid"])["data"];
        for ($i=0; $i < $rowuser["operator"]; $i++) {
          $level = $this->MS_levels->getDataById($rowuser['user_oper'][$i]["level_id"])[0];
          $rowuser['user_oper'][$i]['level'] = $level;
        }
      }
      $rowuser['image'] = $this->path."assets/images/profiles/".$rowuser['image'];
  		$callback = array(
                          "status" => 200,
                          "type" => TRUE,
            						  "msg" => "OK",
            						  "data" => $rowuser
                        );
  	}else{
  		$callback = array(
                          "status" => 404,
                          "type" => FALSE,
                          "msg" => "Not Found",
                          "data" => ""
                        );
  	}
  	return $callback;
  }

  public function getProfileDetail($Id)
  {
    $sql = "SELECT * FROM users_view WHERE uid = '$Id'";
  	$qry = $this->db->query($sql);
    if ($qry) {
      $rowuser = $qry->result_array()[0];
      $rowuser['image'] = $this->path."assets/images/profiles/".$rowuser['image'];
      $callback = array(
                          "status" => 200,
                          "type" => TRUE,
                          "msg" => "OK",
                          "data" => $rowuser
                        );
    }else {
      $callback = array(
                          "status" => 405,
                          "type" => FALSE,
                          "msg" => "Query Error",
                        );
    }
    return $callback;
  }

  public function chk_gender($gender)
  {
  	if ($gender == "F") {
  		$callback = array( "eng" => "Female",
  						   "thai" => "หญิง"
  						 );
  	}elseif ($gender == "M") {
  		$callback = array( "eng" => "Male",
  						   "thai" => "หญิง"
  						 );
  	}else{
  		$callback = array( "eng" => "none",
  						   "thai" => "ไม่ระบุเพศ"
  						 );
  	}
  	return $callback;
  }

  public function chk_duplicate_username($username)
  {
    $sql = "SELECT * FROM users WHERE username = '$username' ";
    $qry = $this->db->query($sql)->num_rows();
    $callback = array();
    if($qry<=0){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                    );
    }else{
                    $callback = array(
                      "status" => 401,
                      "type" => FALSE,
                      "msg" => "Duplicate username",
                    );
    }
    return $callback;
  }

  public function chk_checkpassword($uid,$password)
  {
    $callback = $this->getUserById($uid);
    if ($callback['status'] == 200) {
      if ($callback['data']['password'] == $password) {
        $callback = array(
                            "status" => 200,
                            "type" => FALSE,
                            "msg" => "OK",
                          );
      }else {
        $callback = array(
                            "status" => 402,
                            "type" => FALSE,
                            "msg" => "Password Incorrect",
                          );
      }
    }
    return $callback;
  }

  public function getAlluser($company_id,$branch_id,$dep_id,$site_id,$team_id,$level_id)
  {
    $sql = "SELECT * FROM users_view ";
    $qry = $this->db->query($sql);
  }

  public function getUserService($company_id,$branch_id,$dep_id,$site_id,$team_id)
  {
    if ($company_id != '') {
      $company_id = "WHERE company_id = '".$company_id."' AND user_status = '1'";
    }else {
      $company_id = "WHERE user_status = '1'";
    }
    if ($branch_id != "") {
      $branch_id = "AND branch_id in ('$branch_id')";
    }
    if ($dep_id != "") {
      $dep_id = "AND dep_id in ('$dep_id')";
    }
    if ($site_id != "") {
        $site_id = "AND site_id in ('$site_id')";
    }
    if ($team_id != "") {
        $team_id = "AND team_id in ('$team_id')";
    }
    $sql = "SELECT * FROM users_view  $company_id $branch_id $dep_id $site_id $team_id ";
    $qry = $this->db->query($sql);
    if ($qry) {
      if ($qry->num_rows()>0) {
        $rowuser = $qry->result_array();
        $callback = array(
                            "status" => 200,
                            "type" => TRUE,
                            "msg" => "OK",
                            "data" => $rowuser
                          );
      }else {
        $callback = array(
                            "status" => 404,
                            "type" => FALSE,
                            "msg" => "Not Found",
                            "data" =>$sql
                          );
      }
    }else {
      $callback = array(
                          "status" => 405,
                          "type" => FALSE,
                          "msg" => "Query Error",
                          "data" =>$sql
                        );
    }
    return $callback;
  }
}
