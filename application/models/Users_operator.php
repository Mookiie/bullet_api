<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_operator extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Functions');
    $this->now = $this->Functions->date_time_get();

  }

  private function GenerateOID()
  {
    $sql = "SELECT auto_id FROM users_operator ORDER BY auto_id DESC LIMIT 1";
      $qry = $this->db->query($sql);
        if($qry->num_rows()>0){
          $id = $qry->result_array();
          $oldId =  $id[0]['auto_id'];
          $newNUM = $oldId+1;
          return $newNUM;
        }else{
          $newNUM = "1";
          return $newNUM;
        }
  }

  public function createoperator($uid,$company_id,$branch_id,$dep_id,$site_id,$team_id,$create_by,$level_id)
  {
    $callback = array();
    $Id = $this->GenerateOID();
    $sql_insert = "INSERT INTO users_operator
                   VALUES('$Id','$uid','$company_id','$branch_id','$dep_id','$site_id','$team_id','1','$create_by','$this->now',null,null,'$level_id')";
    $qry  = $this->db->query($sql_insert);
      if($qry){
        $this->Users->updateoperator($uid,$Id);
        $callback = array(
                          "status" => 200,
                          "type" => TRUE,
                          "msg" => "OK",
                          "id" => $Id
                         );
      }else{
        $callback = array(
                          "status" => 400,
                          "type" => FALSE,
                          "msg" => "Insert Failed",
                          );
      }
    return $callback;
  }

  public function getOperation($uid)
  {
    $sql = "SELECT * FROM users_operator WHERE uid = '$uid' AND ub_status = '1'";
    $qry = $this->db->query($sql);
    if ($qry->num_rows() > 0) {
     $oper = $qry->result_array();
     $callback = array(
                         "status" => 200,
                         "type" => TRUE,
                         "msg" => "OK",
                         "data" => $oper
                        );
   }else{
     $callback = array(
                         "status" => 404,
                         "type" => FALSE,
                         "msg" => "Not Found",
                         "data" => ""
                       );
   }
   return $callback;
  }



}
