<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tickets extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Functions');
    $this->now = $this->Functions->date_time_get();
    $this->path = $this->Functions->get_path();
  }

  private function GenerateTID()
  {
    $query = "SELECT ticket_id FROM ticket ORDER BY ticket_id DESC LIMIT 1";
    $qryid = $this->db->query($query);
    if($qryid->num_rows()>0){
      $userId = $qryid->result_array();
      $oldId = $userId[0]["ticket_id"];
      $oldYMD =  substr($oldId,1,6);
      $y = sprintf("%02d",date('y'));
      $m = sprintf("%02d",date('m'));
      $d = sprintf("%02d",date('d'));
      $newYMD =  $y.$m.$d;
      $oldNUM =  sprintf("%06d",substr($oldId,7,6));
      if ($oldYMD == $newYMD) {
          $newNUM = $oldNUM+1;
          $newNUM = sprintf("%06d",$newNUM);
      }else{
          $newNUM = "000001";
      }
      return "T".$newYMD.$newNUM;
    }else{
      $y = sprintf("%02d",date('y'));
      $m = sprintf("%02d",date('m'));
      $d = sprintf("%02d",date('d'));
      $newYMD =  $y.$m.$d;
      $newNUM = "000001";
      return "T".$newYMD.$newNUM;
    }
  }

  private function GenerateWID()
  {
    $query = "SELECT work_id FROM ticket_working ORDER BY work_id DESC LIMIT 1";
    $qryid = $this->db->query($query);
    if($qryid->num_rows()>0){
      $userId = $qryid->result_array();
      $oldId = $userId[0]["work_id"];
      $oldYMD =  substr($oldId,1,6);
      $y = sprintf("%02d",date('y'));
      $m = sprintf("%02d",date('m'));
      $d = sprintf("%02d",date('d'));
      $newYMD =  $y.$m.$d;
      $oldNUM =  sprintf("%06d",substr($oldId,7,6));
      if ($oldYMD == $newYMD) {
          $newNUM = $oldNUM+1;
          $newNUM = sprintf("%06d",$newNUM);
      }else{
          $newNUM = "000001";
      }
      return "W".$newYMD.$newNUM;
    }else{
      $y = sprintf("%02d",date('y'));
      $m = sprintf("%02d",date('m'));
      $d = sprintf("%02d",date('d'));
      $newYMD =  $y.$m.$d;
      $newNUM = "000001";
      return "W".$newYMD.$newNUM;
    }
  }

  private function GenerateDID($ticket_id)
  {
    $query = "SELECT detail_id FROM ticket_detail WHERE ticket_id = '$ticket_id' ORDER BY detail_id DESC LIMIT 1";
    $qryid = $this->db->query($query);
    if($qryid->num_rows()>0){
      $id = $qryid->result_array();
      $oldId =  $id[0]['detail_id'];
      $newNUM = $oldId+1;
      return $newNUM;
    }else{
      $newNUM = "1";
      return $newNUM;
    }
  }

  private function GenerateWNO($work_id)
  {
    $sql = "SELECT work_no FROM ticket_working WHERE work_id = '$work_id' ORDER BY work_no DESC LIMIT 1";
    $qry = $this->db->query($sql);
        if($qry->num_rows()>0){
          $id = $qry->result_array();
          $oldId =  $id[0]['work_no'];
          $newNUM = $oldId+1;
          return $newNUM;
        }else{
          $newNUM = "1";
          return $newNUM;
        }
  }

  public function create($ticket_refer,$ticket_company,$ticket_branch,$ticket_dep,$ticket_site,$ticket_title,$ticket_detail,$ticket_create)
  {
    $callback = array();
    $Id = $this->GenerateTID();
    $sql_insert = "INSERT INTO ticket
                          VALUES('$Id','$ticket_refer','$ticket_company','$ticket_branch','$ticket_dep','$ticket_site','$ticket_title','$ticket_detail','','$ticket_create','$this->now',null,null,'1')";
    $qry  = $this->db->query($sql_insert);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                      "id" => $Id
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function edit($ticket_id,$ticket_title,$ticket_detail,$uid)
  {
    $callback = array();
    $sql = "UPDATE ticket
            SET ticket_title = '$ticket_title',
                ticket_detail = '$ticket_detail',
                ticket_update = '$uid',
                ticket_udate = '$this->now'
            WHERE ticket_id = '$ticket_id'";
    $qry  = $this->db->query($sql);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function file_ticket($ticket_id,$file)
  {
    $imgname = $file["name"][0];
    $tmp = $file["tmp_name"][0];
    $image_array = explode(".",$imgname);
    $type = $image_array[count($image_array)-1];
    if ($type!="png" || $type!="jpg" || $type!="jpeg" || $type!="gif" || $type!="pdf" ) {
      $newname = $ticket_id."_".time().".".$type;
      if (move_uploaded_file($tmp,"assets/documents/Tickets/".$newname)){
        $sql = "UPDATE ticket SET ticket_image = '$newname' WHERE ticket_id ='$ticket_id' ";
        $qry = $this->db->query($sql);
        if ($qry) {
          $callback = array(
            "status" => 200,
            "type" => TRUE,
            "msg" => "OK",
          );
        }else {
          $callback = array(
            "status" => 400,
            "type" => FALSE,
            "msg" => "Insert Failed",
          );
        }
      }else {
        $callback = array(
          "status" => 400,
          "type" => FALSE,
          "msg" => "Upload Failed",
        );
      }
    }else {
      $callback = array(
        "status" => 406,
        "type" => FALSE,
        "msg" => "Type Not Support",
      );
    }
    return $callback;
  }

  public function update_status($Id,$status)
  {
    $callback = array();
    $sql = "UPDATE ticket
            SET ticket_status = '$status'
            WHERE ticket_id = '$Id'";
    $qry  = $this->db->query($sql);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function create_working($work_id,$ticket_id,$work_type,$work_assign,$work_assign_to,$work_finish)
  {
    $callback = array();
    if ($work_id == '') {
      $work_id = $this->GenerateWID();
    }else {
      $sql_update = "UPDATE ticket_working SET work_status = '0' WHERE work_id = '$work_id'";
      $qry_update  = $this->db->query($sql_update);
    }
    $work_no = $this->GenerateWNO($work_id);
    $sql_insert = "INSERT INTO ticket_working
                               VALUES('$ticket_id','$work_id','$work_no','$work_type','$work_assign','$work_assign_to','$this->now','$work_finish','','','1')";
    $qry  = $this->db->query($sql_insert);
    if($qry){
              $this->update_status($ticket_id,'2');
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                      "id" => $work_id
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function working_QC_success($ticket_id,$work_QC_by)
  {
    // $status = $this->chk_workfinish_date($word_id,$work_finish);
    $sql = "UPDATE ticket_working
            SET work_QC_by = '$work_QC_by',
                work_QC_date = '$this->now'
            WHERE ticket_id = '$ticket_id' AND work_status = '1'";
    $qry  = $this->db->query($sql);
    if($qry){
              $this->update_status($ticket_id,'7');
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function working_QC_eject($ticket_id,$work_QC_by,$work_id)
  {
    // $status = $this->chk_workfinish_date($work_id,$work_finish);
    $sql = "UPDATE ticket_working
            SET work_QC_by = '$work_QC_by',
                work_QC_date = '$this->now'
            WHERE ticket_id = '$ticket_id' AND work_status = '1'";
    $qry  = $this->db->query($sql);
    if($qry){
              $this->update_status($ticket_id,'6');
              $this->update_workstatus($work_id,'0');
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function Ticket_Eject($ticket_id,$log_id,$reason)
  {
    $sql = "INSERT INTO ticket_eject VALUES('$ticket_id','$log_id','$this->now','$reason')";
    $qry  = $this->db->query($sql);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function getWorkIdByTicket($ticket_id)
  {
    $sql="SELECT work_id FROM ticket_working WHERE work_status = '1' AND ticket_id = '$ticket_id'";
    $qry = $this->db->query($sql);
    return $qry->result_array()[0]["work_id"];
  }

  // public function chk_workfinish_date($word_id,$work_finish)
  // {
  //   $sql = "SETECT work_finish
  //           FROM ticket_working
  //           WHERE work_id = '$word_id'";
  //   $qry = $this->db->query($sql)->result_array()[0];
  //   $status = "";
  //   if ($qry<$work_finish) {
  //     $status = '2';//delay
  //   }elseif ($qry>=$work_finish) {
  //     $status = '1';//success
  //   }
  //   return $status;
  // }

  public function update_workstatus($word_id,$status)
  {
    $callback = array();
    $sql = "UPDATE ticket_working
            SET work_status = '$status'
            WHERE work_id = '$word_id'";
    $qry  = $this->db->query($sql);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function create_detail($ticket_id,$work_id,$detail_description,$create_by)
  {
    $callback = array();
    $Id = $this->GenerateDID($ticket_id);
    $sql_insert = "INSERT INTO ticket_detail
                          VALUES('$ticket_id', '$work_id', '$Id', '$this->now', '$detail_description', null, '$create_by', null)";
    $qry  = $this->db->query($sql_insert);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                      "id" => $Id
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                        "data" => $sql_insert
                      );
    }
    return $callback;
  }

  public function file_Detail($ticket_id,$detail_id,$file)
  {
    $imgname = $file["name"][0];
    $tmp = $file["tmp_name"][0];
    $image_array = explode(".",$imgname);
    $type = $image_array[count($image_array)-1];
    if ($type!="png" || $type!="jpg" || $type!="jpeg" || $type!="gif") {
      $newname = $ticket_id."_".$detail_id."_".time().".".$type;
      if (move_uploaded_file($tmp,"assets/documents/Details/".$newname)){
        $sql = "UPDATE ticket_detail SET detail_image = '$newname' WHERE ticket_id ='$ticket_id' AND detail_id = '$detail_id' ";
        $qry = $this->db->query($sql);
        if ($qry) {
          $callback = array(
            "status" => 200,
            "type" => TRUE,
            "msg" => "OK",
          );
        }else {
          $callback = array(
            "status" => 400,
            "type" => FALSE,
            "msg" => "Insert Failed",
          );
        }
      }else {
        $callback = array(
          "status" => 400,
          "type" => FALSE,
          "msg" => "Upload Failed",
        );
      }
    }else {
      $callback = array(
        "status" => 406,
        "type" => FALSE,
        "msg" => "Type Not Support",
      );
    }
    return $callback;
  }

  public function chk_ticket_status($status)
  {
    switch ($status) {
        case "1":
            $status = "wait";
            break;
        case "2":
            $status = "assign";
            break;
        case "3":
            $status = "dismiss";
            break;
        case "4":
            $status = "working";
            break;
        case "5":
            $status = "check";
            break;
        case "6":
            $status = "cancel";
            break;
        case "7":
            $status = "success";
            break;
        default:
            $status = "error";
    }

    return $status;
  }

  public function getTicketsByTicketID($ticket_id)
  {
    $sql = "SELECT * FROM ticket_view where ticket_id = '$ticket_id'";
    $qry=$this->db->query($sql);
    return $qry->result_array()[0];
  }

  public function getCountTicketMeByStatus($ticket_create,$status)
  {
    if ($status != "") {
        $status = "AND ticket_status in $status";
    }
    $sql="SELECT * FROM ticket WHERE ticket_create = '$ticket_create' $status";
    $qry=$this->db->query($sql);
    return $qry->num_rows();
  }

  public function getTicket_me($ticket_create,$status)
  {
    if ($status != "") {
      $status = "AND ticket_status in $status ";
    }
    $sql="SELECT * FROM ticket LEFT JOIN users on ticket.ticket_create = users.uid WHERE ticket_create = '$ticket_create' $status ORDER BY ticket_date DESC";
    $qry=$this->db->query($sql);
    $callback = array();
    if ($qry) {
      if ($qry->num_rows()>0) {
        $callback = array(
                            "status" => 200,
                            "type" => TRUE,
                            "msg" => "OK",
                            "data" => $qry->result_array()
                          );
      }else {
        $callback = array(
                            "status" => 404,
                            "type" => FALSE,
                            "msg" => "Not Found",
                          );
      }
    }else {
      $callback = array(
                          "status" => 405,
                          "type" => FALSE,
                          "msg" => "Query Error",
                          "data" => $sql
                        );
    }
    return $callback;
  }

  public function getCountTicketServiceByStatus($company_id,$branch_id,$dep_id,$site_id,$status)
  {
    if ($company_id != '') {
      $company_id = "WHERE ticket_company = '".$company_id."'";
      if ($status != "") {
        $status = "AND ticket_status in $status";
      }
    }else {
      if ($status != "") {
        $status = "WHERE ticket_status in $status";
      }
    }
    if ($branch_id != "''") {
    $branch_id = "AND ticket_branch in ($branch_id)";
    }else{$branch_id = "";}
    if ($dep_id != "''") {
      $dep_id = "AND ticket_dep in ($dep_id)";
    }else{$dep_id = "";}
    if ($site_id != "''") {
      $site_id = "AND ticket_site in ($site_id)";
    }else{$site_id = "";}

    $sql="SELECT * FROM ticket $company_id $status $branch_id $dep_id $site_id ";
    $qry=$this->db->query($sql);
    return $qry->num_rows();
  }

  public function getTicketService($company_id,$branch_id,$dep_id,$site_id,$status)
  {
    if ($company_id != '') {
      $company_id = "WHERE ticket_company = '".$company_id."'";
      if ($status != "") {
        $status = "AND ticket_status in $status";
      }
    }else {
      if ($status != "") {
        $status = "WHERE ticket_status in $status";
      }
    }
    if ($branch_id != "''") {
    $branch_id = "AND ticket_branch in ($branch_id)";
    }else{$branch_id = "";}
    if ($dep_id != "''") {
      $dep_id = "AND ticket_dep in ($dep_id)";
    }else{$dep_id = "";}
    if ($site_id != "''") {
        $site_id = "AND ticket_site in ($site_id)";
    }else{$site_id="";}

    $sql="SELECT * FROM ticket LEFT JOIN users on ticket.ticket_create = users.uid
    $company_id $status $branch_id $dep_id $site_id ORDER BY ticket_date DESC";
    $qry=$this->db->query($sql);
    if ($qry) {
      if ($qry->num_rows()>0) {
        $rowticket = $qry->result_array();
        $callback = array(
                            "status" => 200,
                            "type" => TRUE,
                            "msg" => "OK",
                            "data" => $rowticket
                          );
      }else {
        $callback = array(
                            "status" => 404,
                            "type" => FALSE,
                            "msg" => "Not Found",
                          );
      }
    }else {
      $callback = array(
                          "status" => 405,
                          "type" => FALSE,
                          "msg" => "Query Error",
                          "data" => $sql
                        );
    }
    return $callback;
  }

  public function getTicketDetail($ticket_id)
  {
    $sql = "SELECT * FROM ticket_view where ticket_id = '$ticket_id'";
    $qry=$this->db->query($sql);
    if ($qry) {
      $rowticket = $qry->result_array()[0];
      if ($rowticket['ticket_image']!= '') {
        $rowticket['ticket_image'] = $this->path."assets/documents/Tickets/".$rowticket['ticket_image'];
      }
      if ($rowticket['ticket_refer_image']!= '') {
        $rowticket['ticket_refer_image'] = $this->path."assets/documents/Tickets/".$rowticket['ticket_refer_image'];
      }
      $callback = array(
                          "status" => 200,
                          "type" => TRUE,
                          "msg" => "OK",
                          "data" => $rowticket
                        );
    }else {
      $callback = array(
                          "status" => 405,
                          "type" => FALSE,
                          "msg" => "Query Error",
                          "data" => $sql
                        );
    }
    return $callback;
  }

  public function getTicketTracking($ticket_id)
  {
    $sql = "SELECT * FROM tracking_view where passive = '$ticket_id' ORDER BY log_date DESC";
    $qry=$this->db->query($sql);
    if ($qry) {
      if ($qry->num_rows()>0) {
        $rowticket = $qry->result_array();
        for ($i=0; $i < count($rowticket); $i++) {
          $rowticket[$i]['active_image'] = $this->path."assets/images/profiles/".$rowticket[$i]['active_image'];
          if ($rowticket[$i]['passive_image'] != '') {
            $rowticket[$i]['passive_image'] = $this->path."assets/images/profiles/".$rowticket[$i]['passive_image'];
          }
        }
        $callback = array(
                            "status" => 200,
                            "type" => TRUE,
                            "msg" => "OK",
                            "data" => $rowticket
                          );
      }else {
        $callback = array(
                            "status" => 404,
                            "type" => FALSE,
                            "msg" => "Not Found",
                          );
      }
    }else {
      $callback = array(
                          "status" => 405,
                          "type" => FALSE,
                          "msg" => "Query Error",
                          "data" => $sql
                        );
    }
    return $callback;
  }

  public function getCountTicketWork($company_id,$branch_id,$dep_id,$site_id,$status,$uid)
  {
    if ($company_id != '') {
      $company_id = "WHERE ticket_company = '".$company_id."'";
      if ($status != "") {
        $status = "AND ticket_status in $status";
      }
    }else {
      if ($status != "") {
        $status = "WHERE ticket_status in $status";
      }
    }
    if ($branch_id != "''") {
    $branch_id = "AND ticket_branch in ($branch_id)";
    }else{$branch_id = "";}
    if ($dep_id != "''") {
      $dep_id = "AND ticket_dep in ($dep_id)";
    }else{$dep_id = "";}
    if ($site_id != "''") {
      $site_id = "AND ticket_site in ($site_id)";
    }else{$site_id = "";}

    $sql="SELECT * FROM ticket_view $company_id $status $branch_id $dep_id $site_id ";
    $qry=$this->db->query($sql);
    return $qry->num_rows();
  }
  // stay
  public function getTicketWork($company_id,$branch_id,$dep_id,$site_id,$status,$uid)
  {
    if ($company_id != '') {
      $company_id = "WHERE ticket_company = '".$company_id."'";
      if ($status != "") {
        $status = "AND ticket_status in $status";
      }
    }else {
      if ($status != "") {
        $status = "WHERE ticket_status in $status";
      }
    }
    if ($branch_id != "''") {
    $branch_id = "AND ticket_branch in ($branch_id)";
    }else{$branch_id = "";}
    if ($dep_id != "''") {
      $dep_id = "AND ticket_dep in ($dep_id)";
    }else{$dep_id = "";}
    if ($site_id != "''") {
      $site_id = "AND ticket_site in ($site_id)";
    }else{$site_id = "";}
    // WHERE work_assign_to = '$uid'

    $sql="SELECT * FROM ticket_view $company_id $status $branch_id $dep_id $site_id ";
    $qry=$this->db->query($sql);
    if ($qry) {
      if ($qry->num_rows()>0) {
        $rowticket = $qry->result_array();
        $callback = array(
                            "status" => 200,
                            "type" => TRUE,
                            "msg" => "OK",
                            "data" => $rowticket
                          );
      }else {
        $callback = array(
                            "status" => 404,
                            "type" => FALSE,
                            "msg" => "Not Found",
                            "data" => $sql
                          );
      }
    }else {
      $callback = array(
                          "status" => 405,
                          "type" => FALSE,
                          "msg" => "Query Error",
                          "data" => $sql
                        );
    }
    return $callback;
  }

  public function getTicketDetailWork($ticket_id)
  {
    $sql = "SELECT * FROM db_service.ticket_detail d LEFT JOIN users u ON d.create_by = u.uid WHERE d.ticket_id = '$ticket_id' ORDER BY detail_date DESC";
    $qry=$this->db->query($sql);
    if ($qry) {
      if ($qry->num_rows()>0) {
        $rowticket = $qry->result_array();
        for ($i=0; $i < count($rowticket); $i++) {
          $rowticket[$i]['image'] = $this->path."assets/images/profiles/".$rowticket[$i]['image'];
          if ($rowticket[$i]['detail_image'] != '') {
            $rowticket[$i]['detail_image'] = $this->path."assets/images/profiles/".$rowticket[$i]['detail_image'];
          }
        }
        $callback = array(
                            "status" => 200,
                            "type" => TRUE,
                            "msg" => "OK",
                            "data" => $rowticket,
                            "count" =>count($rowticket)
                          );
      }else {
        $callback = array(
                            "status" => 404,
                            "type" => FALSE,
                            "msg" => "Not Found",
                          );
      }
    }else {
      $callback = array(
                          "status" => 405,
                          "type" => FALSE,
                          "msg" => "Query Error",
                          "data" => $sql
                        );
    }
    return $callback;
  }



}
