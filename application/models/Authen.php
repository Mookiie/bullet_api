<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Authen extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Functions');
    $this->now = $this->Functions->date_time_get();
    $this->table = "log_users";
  }

  public function Login($username,$password)
  {
  	$rowuser = $this->Users->getUserByUsername($username);
  	if ($rowuser['status'] == 200 ) {

  		if ($rowuser['data']['user_status'] == '1') {
  			if ($rowuser['data']['password'] == $password) {
  				$key = md5("ch1nn@p@T.J");
          $rowuser_oper = $this->Users_operator->getOperation($rowuser["data"]["uid"]);
          $oper = array();
          if ($rowuser_oper['status'] == 200) {
          $oper = array('data'=>$rowuser_oper['data']);
          }
         $uid = $rowuser['data']['uid'];

         $sql_level = "SELECT * FROM db_service.users_view WHERE uid = '$uid' ORDER BY level_id ASC limit 1";
         $qry_level = $this->db->query($sql_level)->result_array()[0]['level_id'];
         $level = $this->MS_levels->getDataById($qry_level);
                $tokenData = array(
                        "uid" => $rowuser["data"]["uid"],
                        "username" => $rowuser["data"]["username"],
        				        "email" => $rowuser["data"]["email"],
                        "company_id" => $rowuser["data"]["company_id"],
                        "branch_id" => $rowuser["data"]["branch_id"],
        				        "dep_id" => $rowuser["data"]["dep_id"],
        				        "site_id" => $rowuser["data"]["site_id"],
        				        "team_id" => $rowuser["data"]["team_id"],
        				        "level_id" => $qry_level,
                        "level" => $level,
        				        "admin" => $rowuser["data"]["admin"],
        				        "operator" => $rowuser["data"]["operator"],
                        "user_oper" => $oper,
        				        "user_status" => $rowuser["data"]["user_status"],
                        "iat" => time(),
                      );
                $jwt = JWT::encode($tokenData, $key);
                // $token = AUTHORIZATION::generateToken($tokenData);
                // print_r($token);exit;
  				$callback = array(
  									"status" => 200,
			                        "type" => TRUE,
			                        "msg" => "OK",
			                        "token" => $jwt,
			                        "channel" => md5($rowuser["data"]["uid"]),
  								 );
          $this->Functions->insertLog($this->table,$rowuser["data"]["company_id"],"login","เข้าสู่ระบบ",$rowuser["data"]["uid"],"","","","users");
  				return $callback;
  			}
  			else{
  				$callback = array(
  									"status" => 402,
			                        "type" => FALSE,
			                        "msg" => "Password Incorrect",
			                        "token" => "",
  								 );
          $this->Functions->insertLog($this->table,$rowuser["data"]["company_id"],"Password Incorrect","รหัสผ่านผิด",$rowuser["data"]["uid"],"","","","users");
  				return $callback;
  			}
  		}elseif ($rowuser['data']['user_status'] == '9') {
  			$callback = array(
                          "status" => 501,
                          "type" => FALSE,
                          "msg" => "Wait Conform",
                        );
        $this->Functions->insertLog($this->table,$rowuser["data"]["company_id"],"Wait Conform","รอคอนเฟิร์ม",$rowuser["data"]["uid"],"","","","users");
  			return $callback;
  		}else{
  			$callback = array(
                          "status" => 502,
                          "type" => FALSE,
                          "msg" => "Not Active",
                        );
        $this->Functions->insertLog($this->table,$rowuser["data"]["company_id"],"Not Active","ถูกปิดการใช้งาน",$rowuser["data"]["uid"],"","","","users");
  			return $callback;
  		}
  	}else{
      return $rowuser;
  	}
  }

  public function Logout($token)
  {
    $callback = array(
                        "status" => 200,
                        "type" => TRUE,
                        "msg" => "OK",
                     );
    $this->Functions->insertLog($this->table,$token->company_id,"logout","ออกจากระบบ",$token->uid,"","","","users");
    return $callback;
  }

  public function resetPassword($uid,$password)
  {

  }

}
