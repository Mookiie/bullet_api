<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MS_levels extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Functions');
    $this->now = $this->Functions->date_time_get();

  }

  	private function GenerateID()
    {
    	$sql = "SELECT level_id FROM ms_level ORDER BY level_id DESC LIMIT 1";
        $qry = $this->db->query($sql);
          if($qry->num_rows()>0){
          	$lv = $qry->result_array();
            $oldId =  substr($lv[0]['level_id'],4,1);
            $newNUM = sprintf("%04d",$oldId+1);
            return "L".$newNUM;
          }else{
            $newNUM = "0001";
            return "L".$newNUM;
          }
    }

    public function chk_duplicate_name_EN($name_EN)
    {
    	$sql = "SELECT * FROM ms_level WHERE level_name_en = '$name_EN'";
      	$qry = $this->db->query($sql)->num_rows();
      	$callback = array();
      	if($qry<=0){
                        $callback = array(
                          "status" => 200,
                          "type" => TRUE,
                          "msg" => "OK",
                        );
      	}else{
                        $callback = array(
                          "status" => 401,
                          "type" => FALSE,
                          "msg" => "Duplicate name_EN",
                        );
      	}
      	return $callback;
    }

    public function chk_duplicate_name_TH($name_TH)
    {
    	$sql = "SELECT * FROM ms_level WHERE level_name_th = '$name_EN'";
      	$qry = $this->db->query($sql)->num_rows();
      	$callback = array();
      	if($qry<=0){
                        $callback = array(
                          "status" => 200,
                          "type" => TRUE,
                          "msg" => "OK",
                        );
      	}else{
                        $callback = array(
                          "status" => 401,
                          "type" => FALSE,
                          "msg" => "Duplicate name_EN",
                        );
      	}
      	return $callback;
    }

    public function create($name_TH,$name_EN,$ticket,$assign,$qc,$admin,$config,$company_id)
    {
    	$callback = array();
    	$Id = $this->GenerateID();
    	$sql_insert = "INSERT INTO ms_level
                               VALUES('$Id','$name_TH','$name_EN','$ticket','$assign','$qc','$admin','$config','$company_id')";
      	$qry  = $this->db->query($sql_insert);
      	if($qry){
                        $callback = array(
                          "status" => 200,
                          "type" => TRUE,
                          "msg" => "OK",
                          "id" => $Id
                        );
     	 }else{
                        $callback = array(
                          "status" => 400,
                          "type" => FALSE,
                          "msg" => "Insert Failed",
                        );
      	}
    	return $callback;
    }

    public function edit($Id,$name_TH,$name_EN,$ticket,$assign,$qc,$admin,$config)
    {
      $oldData = $this->getDataById($Id);
    	$sql = "UPDATE ms_level
    			SET level_name_th = '$name_TH',level_name_en = '$name_EN',level_ticket = '$ticket',level_assign = '$assign',level_qc = '$qc',level_admin = '$admin' ,level_config = '$config'
    			WHERE level_id = '$Id'";
    	$qry  = $this->db->query($sql);
      	if($qry){
                        $callback = array(
                          "status" => 200,
                          "type" => TRUE,
                          "msg" => "OK",
                          "data" => $oldData[0]
                        );
      	}else{
                        $callback = array(
                          "status" => 400,
                          "type" => FALSE,
                          "msg" => "Insert Failed",
                        );
      	}
    	return $callback;
    }

    public function getDataByRoot($company_id)
    {
    	if ($company_id == "") {
    		$company_id = "WHERE company_id ='".$company_id."'";
    	};
    	$sql = "SELECT * FROM ms_level $company_id";
    	$qry = $this->db->query($sql);
    	return $qry->result_array();
    }

    public function getDataById($Id)
    {
      $sql = "SELECT * FROM ms_level WHERE level_id = '$Id'";
      $qry = $this->db->query($sql);
    	return $qry->result_array();
    }


}
