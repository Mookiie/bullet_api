<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Functions');
    $this->now = $this->Functions->date_time_get();

  }

    private function GenerateID()
    {
    	$sql = "SELECT company_id FROM company ORDER BY company_id DESC LIMIT 1";
      $qry = $this->db->query($sql);
          if($qry->num_rows()>0){
            $data = $qry->result_array();
            $oldId = $data[0]["company_id"];
            $oldYMD =  substr($oldId,1,6);
            $y = sprintf("%02d",date('y'));
            $m = sprintf("%02d",date('m'));
            $d = sprintf("%02d",date('d'));
            $newYMD =  $y.$m.$d;
            $oldNUM =  sprintf("%06d",substr($oldId,7,6));
            if ($oldYMD == $newYMD) {
                $newNUM = $oldNUM+1;
                $newNUM = sprintf("%06d",$newNUM);
            }else{
                $newNUM = "000001";
            }
            return "C".$newYMD.$newNUM;
          }else{
            $y = sprintf("%02d",date('y'));
            $m = sprintf("%02d",date('m'));
            $d = sprintf("%02d",date('d'));
            $newYMD =  $y.$m.$d;
            $newNUM = "000001";
            return "C".$newYMD.$newNUM;
          }
    }

    public function create($name_TH,$name_EN)
    {
      $callback = array();
    	$Id = $this->GenerateID();
      $sql = "INSERT INTO company
                             VALUES('$Id','$name_TH','$name_EN','$this->now','1')";
      $qry = $this->db->query($sql);
      if($qry){
                        $callback = array(
                          "status" => 200,
                          "type" => TRUE,
                          "msg" => "OK",
                          "id" => $Id,
                        );
      }else{
                        $callback = array(
                          "status" => 400,
                          "type" => FALSE,
                          "msg" => "Insert Failed",
                          "id" => '',
                        );
      }
    	return $callback;
    }

    public function chk_duplicate_name_TH($name_TH)
    {
      $sql = "SELECT * FROM company WHERE company_name_th = '$name_TH'";
      $qry = $this->db->query($sql)->num_rows();
      $callback = array();
      if($qry<=0){
                        $callback = array(
                          "status" => 200,
                          "type" => TRUE,
                          "msg" => "OK",
                        );
      }else{
                        $callback = array(
                          "status" => 401,
                          "type" => FALSE,
                          "msg" => "Duplicate name_TH",
                        );
      }
      return $callback;
    }

    public function chk_duplicate_name_EN($name_EN)
    {
      $sql = "SELECT * FROM company WHERE company_name_en = '$name_EN'";
      $qry = $this->db->query($sql)->num_rows();
      $callback = array();
      if($qry<=0){
                        $callback = array(
                          "status" => 200,
                          "type" => TRUE,
                          "msg" => "OK",
                        );
      }else{
                        $callback = array(
                          "status" => 401,
                          "type" => FALSE,
                          "msg" => "Duplicate name_EN",
                        );
      }
      return $callback;
    }

    public function getCompany($status)
    {
      if ($status != "") {
        $status = "WHERE company_status = '".$status."'";
      }
      $sql = "SELECT * FROM company $status ";
      $qry = $this->db->query($sql);
      $callback = $qry->result_array();
      return $callback;
    }


}
