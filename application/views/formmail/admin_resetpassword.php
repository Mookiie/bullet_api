<!DOCTYPE html>
  <html>
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      </head>
      <body style="margin:0px;padding:0px;">
        <div style="width:100%;height:100%;background:#eceff1;position:absolute;text-align:center;">
              <div style="box-shadow:0px 1px 1px 0px rgba(0,0,0,.1);position:absolute;width:90%;height:calc(100% - 100px);margin-top:50px;margin-left:5%;background:#fff;">
                   <div  style= "height:150px; width:100%; color:#fff;
                                font-family: 'Prompt', sans-serif; text-align:center;
                                font-weight:light; background-color:#fff;">
                                <img style="margin-top:10px;" height="120px"
                                src="http://203.151.43.169/digidoc//assets/images/logo/logo_wha.png"
                                >
                   </div>
                   <div style="text-align:left;width:100%;color:#455a64;">
                        <h1 style="width:100%; display:block; text-align:center; color:#37474f;">คำขอรีเซ็ตรหัสผ่าน</h1>
                        <p style="margin:5px 30px;">
                          เรียน  <?php echo $name; ?>, <br />
                        </p>

                        <p style="margin:15px 30px;">รีเซ็ตรหัสผ่าน Digidoc ของคุณเรียบร้อยแล้ว<br />
                       คุณสามารถเข้าใช้งานบัญชีของคุณโดยใช้ <br />
                       <br />
                       <p style="margin:5px 30px;">
                        Username : <?php echo $username; ?> <br />
                        Password : <?php echo $password; ?>
                       </p>
                       <br />
                       <p style="margin:5px 30px;">
                        ขอแสดงความนับถือ <br />
                        Digidoc by Siamrajathanee
                      </p>

                   </div>
             <hr />
             <div style="text-align:left;width:100%;color:#455a64;">
               <p style="text-align:center; padding:20px;">
                  หากมีข้อสงสัยหรือสอบถามเพิ่มเติม ติดต่อ: <a href=<?php echo "http://".$_SERVER['SERVER_NAME']."/digidoc_wha" ?> >Help Center</a>
               </p>
            </div>
        </div>
        <div style="position:absolute;bottom:0; width:100%; text-align:center; left:0; padding:10px 0px;">
              <p style="color:#546e7a;">
                Thailand | Siamrajathanee co., ltd Sumutpraparn 10130 | Copyright &copy; 2018
              </p>
        </div>
  	 </div>
		</body>
</html>
