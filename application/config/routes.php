<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//login-logout
$route['login'] = 'Authen_controller/Login';
$route['logout'] = 'Authen_controller/Logout';
$route['testmail'] = 'Authen_controller/testmail';

// company
$route['company/create'] = 'Company_controller/create';
$route['company/getdata'] = 'Company_controller/getDatacompany';

// user
$route['user/create_user'] = 'Users_controller/create_user';
$route['user/create_operator'] = 'Users_controller/create_operator';
$route['user/profile'] = 'Users_controller/Profile';
$route['user/profiledetail'] = 'Users_controller/ProfileDetail';
$route['user/getuserservice'] = 'Users_controller/UserService';

// ticket
$route['ticket/create_ticket'] = 'Tickes_controller/create_tickets';//1
$route['ticket/ticket_files'] = 'Tickes_controller/tickets_files';
$route['ticket/assign_working'] = 'Tickes_controller/assign_working';//2
$route['ticket/countstatusme'] = 'Tickes_controller/CountTicketMeStatus';
$route['ticket/me'] = 'Tickes_controller/TicketMe';
$route['ticket/countservicestatus'] = 'Tickes_controller/CountTicketServiceStatus';
$route['ticket/service'] = 'Tickes_controller/TicketService';
$route['ticket/countwork'] = 'Tickes_controller/CountTicketWorkStatus';
$route['ticket/work'] = 'Tickes_controller/TicketWork';
$route['ticket/profile'] = 'Tickes_controller/TicketProfile';
$route['ticket/tracking'] = 'Tickes_controller/TicketTracking';
$route['ticket/dismissassign'] = 'Tickes_controller/Ticket_assigndismiss';//3
$route['ticket/dismisswork'] = 'Tickes_controller/Ticket_workdismiss';//3
$route['ticket/dismissuser'] = 'Tickes_controller/Ticket_userdismiss';//3
$route['ticket/working'] = 'Tickes_controller/Ticket_working';
$route['ticket/download'] = 'Tickes_controller/TicketDownload';
$route['ticket/detail'] = 'Tickes_controller/Ticket_Detail';
$route['ticket/detail_file'] = 'Tickes_controller/Detail_files';
$route['ticket/detail_work'] = 'Tickes_controller/TicketDetailWork';
$route['ticket/working_success'] = 'Tickes_controller/TicketWorkingSuccess';//5
$route['ticket/eject'] = 'Tickes_controller/Ticket_Eject';//6
// $route['ticket/reTicket'] = 'Tickes_controller/Ticket_ReTicket';//6->1
$route['ticket/success'] = 'Tickes_controller/Ticket_success';//7
$route['ticket/edit_ticket'] = 'Tickes_controller/Ticket_edit';


// master file
$route['level/create'] = 'Master_controller/create_level';
$route['level/edit'] = 'Master_controller/edit_level';
$route['level/getdata'] = 'Master_controller/data_level';

$route['branch/create'] = 'Master_controller/create_branch';
$route['branch/edit'] = 'Master_controller/edit_branch';
$route['branch/drop'] = 'Master_controller/drop_branch';
$route['branch/status'] = 'Master_controller/status_branch';
$route['branch/getdata'] = 'Master_controller/data_branch';

$route['dep/create'] = 'Master_controller/create_department';
$route['dep/edit'] = 'Master_controller/edit_department';
$route['dep/drop'] = 'Master_controller/drop_department';
$route['dep/status'] = 'Master_controller/status_department';
$route['dep/getdata'] = 'Master_controller/data_department';

$route['site/create'] = 'Master_controller/create_site';
$route['site/edit'] = 'Master_controller/edit_site';
$route['site/drop'] = 'Master_controller/drop_site';
$route['site/status'] = 'Master_controller/status_site';
$route['site/getdata'] = 'Master_controller/data_site';

$route['team/create'] = 'Master_controller/create_team';
$route['team/edit'] = 'Master_controller/edit_team';
$route['team/drop'] = 'Master_controller/drop_team';
$route['team/status'] = 'Master_controller/status_team';
$route['team/getdata'] = 'Master_controller/data_team';
$route['team/getdatabysite'] = 'Master_controller/dataBysite_team';
