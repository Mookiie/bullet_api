<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Authen_controller extends REST_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Email');
	}

	public function Login_post()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$callback = $this->Authen->Login($username,$password);
		$this->output->set_output(json_encode($callback));
	}

	public function Logout_post()
	{
		$callback = array();
		if($_SERVER["REQUEST_METHOD"] != "OPTIONS"){
			$token = $_SERVER["HTTP_TOKEN"];
			$user = $this->Token->decode($token);
			$callback = $this->Authen->Logout($user);
		}
		else{
			$callback = array(
												"status" => 404,
		                    "type" => FALSE,
		                    "msg" => "Not Found",
							 				 );
		}
		$this->output->set_output(json_encode($callback));
	}
	public function testmail_post()
	{
		$callback = $this->Email->SendMailConfirmResetPassword("","");
		$this->output->set_output(json_encode($callback));

	}


}
