<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Tickes_controller extends REST_Controller {

	public function __construct(){
		parent::__construct();
		if($_SERVER["REQUEST_METHOD"] != "OPTIONS"){
            $callback["options"] = false;
            $token = $_SERVER["HTTP_TOKEN"];
            $user = $this->Token->decode($token);
						$this->company_id = $user->company_id;
						$this->uid = $user->uid;
						$this->branch_id = $user->branch_id;
						$this->dep_id = $user->dep_id;
						$this->site_id = $user->site_id;
						$this->operator = $user->operator;
						// $this->uid->level_assign = $user->level->level_assign;
						// $this->uid->level_qc = $user->level->level_qc;
						// $this->uid->level_report = $user->level->level_report;
						// $this->uid->level_admin = $user->level->level_admin;
						// $this->uid->level_config = $user->level->level_config;
						if ($this->operator == 1) {
							$this->user_oper = $user->user_oper->data;
						}

						// print_r($user);exit;
						$this->tabel = 'log_ticket';
        }else{
            exit;
        }
	}

	public function create_tickets_post()
	{
		$ticket_refer = $this->input->post('ticket_refer');
		$ticket_company = $this->input->post('ticket_company');
		if ($ticket_company == '') {
			$ticket_company = $this->company_id;
		}
		$ticket_branch = $this->input->post('ticket_branch');
		$ticket_dep = $this->input->post('ticket_dep');
		$ticket_site = $this->input->post('ticket_site');
		$ticket_title = $this->input->post('ticket_title');
		$ticket_detail = $this->input->post('ticket_detail');
		$callback = $this->Tickets->create($ticket_refer, $ticket_company, $ticket_branch, $ticket_dep, $ticket_site,$ticket_title,$ticket_detail,$this->uid);
		if ($callback["status"] == 200) {
			$this->Functions->insertLog($this->tabel,$this->company_id,'create','สร้างคำขอ',$this->uid,$callback["id"],'','','ticket');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function tickets_files_post()
	{
		$file = $_FILES['file'];
		$ticket_id = $_POST['id'];
		$callback = $this->Tickets->file_ticket($ticket_id,$file);
		$this->output->set_output(json_encode($callback));
	}

	// public function create_working_post()
	// {
	// 	$ticket_id = $this->input->post('ticket_id');
	// 	$work_type = $this->input->post('work_type');
	// 	$work_assign_to = $this->input->post('work_assign_to');
	// 	$work_finish = $this->input->post('work_finish');
	// 	$callback = $this->Tickets->create_working($ticket_id,$work_type,$work_assign,$work_assign_to,$work_finish);
	// 	$this->output->set_output(json_encode($callback));
	// }

	public function CountTicketMeStatus_get()
	{
		$status = $this->input->get('status');
		$callback = $this->Tickets->getCountTicketMeByStatus($this->uid,$status);
		$this->output->set_output(json_encode($callback));
	}

	public function TicketMe_get()
	{
		$status = $this->input->get('status');
		$callback = $this->Tickets->getTicket_me($this->uid,$status);
		$this->output->set_output(json_encode($callback));
	}

	public function CountTicketWorkStatus_get()
	{
		$status = $this->input->get('status');
		$company = $this->input->get('company_id');
		if ($company == '') {
			$company = $this->company_id;
		}
		$branch = "'".$this->branch_id."'";
		$dep = "'".$this->dep_id."'";
		$site = "'".$this->site_id."'";
		// เป็น operator
		$rowuser_oper = $this->Users_operator->getOperation($this->uid);
		if ($this->operator == 1) {
			for ($i=0; $i < count($this->user_oper); $i++) {
				$branch .= ",'".$this->user_oper[$i]->branch_id."'";
				if ($this->user_oper[$i]->dep_id != '') {
					$dep		.= ",'".$this->user_oper[$i]->dep_id."'";
				}
				if ($this->user_oper[$i]->site_id != '') {
					$site 	.= ",'".$this->user_oper[$i]->site_id."'";
				}
			}
		}
		// เป็น operator
		$callback = $this->Tickets->getCountTicketWork($company,$branch,$dep,$site,$status,$this->uid);
		$this->output->set_output(json_encode($callback));
	}

	public function TicketWork_get()
	{
		$status = $this->input->get('status');
		$company = $this->input->get('company_id');
		if ($company == '') {
			$company = $this->company_id;
		}
		$rowuser_oper = $this->Users_operator->getOperation($this->uid);
		$branch = "'".$this->branch_id."'";
		$dep = "'".$this->dep_id."'";
		$site = "'".$this->site_id."'";
		if ($this->operator == 1) {
			for ($i=0; $i < count($this->user_oper); $i++) {
				$branch .= ",'".$this->user_oper[$i]->branch_id."'";
				if ($this->user_oper[$i]->dep_id != '') {
					$dep		.= ",'".$this->user_oper[$i]->dep_id."'";
				}
				if ($this->user_oper[$i]->site_id != '') {
					$site 	.= ",'".$this->user_oper[$i]->site_id."'";
				}
			}
		}
		$callback = $this->Tickets->getTicketWork($company,$branch,$dep,$site,$status,$this->uid);
		$this->output->set_output(json_encode($callback));
	}

	public function CountTicketServiceStatus_get()
	{
		$status = $this->input->get('status');
		$company = $this->input->get('company_id');
		if ($company == '') {
			$company = $this->company_id;
		}
		$branch = "'".$this->branch_id."'";
		$dep = "'".$this->dep_id."'";
		$site = "'".$this->site_id."'";
		// เป็น operator
		$rowuser_oper = $this->Users_operator->getOperation($this->uid);
		if ($this->operator == 1) {
			for ($i=0; $i < count($this->user_oper); $i++) {
				$branch .= ",'".$this->user_oper[$i]->branch_id."'";
				if ($this->user_oper[$i]->dep_id != '') {
					$dep		.= ",'".$this->user_oper[$i]->dep_id."'";
				}
				if ($this->user_oper[$i]->site_id != '') {
					$site 	.= ",'".$this->user_oper[$i]->site_id."'";
				}
			}
		}
		// เป็น operator
		$callback = $this->Tickets->getCountTicketServiceByStatus($company,$branch,$dep,$site,$status);
		$this->output->set_output(json_encode($callback));
	}

	public function TicketService_get()
	{
		$status = $this->input->get('status');
		$company = $this->input->get('company_id');
		if ($company == '') {
			$company = $this->company_id;
		}
		$rowuser_oper = $this->Users_operator->getOperation($this->uid);
		$branch = "'".$this->branch_id."'";
		$dep = "'".$this->dep_id."'";
		$site = "'".$this->site_id."'";
		if ($this->operator == 1) {
			for ($i=0; $i < count($this->user_oper); $i++) {
				$branch .= ",'".$this->user_oper[$i]->branch_id."'";
				if ($this->user_oper[$i]->dep_id != '') {
					$dep		.= ",'".$this->user_oper[$i]->dep_id."'";
				}
				if ($this->user_oper[$i]->site_id != '') {
					$site 	.= ",'".$this->user_oper[$i]->site_id."'";
				}
			}
		}
		$callback = $this->Tickets->getTicketService($company,$branch,$dep,$site,$status);
		$this->output->set_output(json_encode($callback));
	}

	public function TicketProfile_get()
	{
		$ticket_id = $this->input->get('ticket_id');
		$callback = $this->Tickets->getTicketDetail($ticket_id);
		$this->output->set_output(json_encode($callback));
	}

	public function Ticket_assigndismiss_get()
	{
		$ticket_id = $this->input->get('ticket_id');
		$callback = $this->Tickets->update_status($ticket_id,'3');
		if ($callback["status"] == 200) {
			if ($this->company_id == '') {
				$this->company_id = $this->Tickets->getTicketsByTicketID($ticket_id)["ticket_company"];
			}
			$this->Functions->insertLog($this->tabel,$this->company_id,'dismiss','ปฏิเสธงาน',$this->uid,$ticket_id,'','','ticket');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function Ticket_userdismiss_get()
	{
		$ticket_id = $this->input->get('ticket_id');
		$callback = $this->Tickets->update_status($ticket_id,'3');
		if ($callback["status"] == 200) {
			if ($this->company_id == '') {
				$this->company_id = $this->Tickets->getTicketsByTicketID($ticket_id)["ticket_company"];
			}
			$this->Functions->insertLog($this->tabel,$this->company_id,'dismiss','ยกเลิกคำขอ',$this->uid,$ticket_id,'','','ticket');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function Ticket_workdismiss_get()
	{
		$ticket_id = $this->input->get('ticket_id');
		$callback = $this->Tickets->update_status($ticket_id,'1');
		if ($callback["status"] == 200) {
			if ($this->company_id == '') {
				$this->company_id = $this->Tickets->getTicketsByTicketID($ticket_id)["ticket_company"];
			}
			$this->Functions->insertLog($this->tabel,$this->company_id,'dismiss','ปฏิเสธรับงาน',$this->uid,$ticket_id,'','','ticket');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function Ticket_working_get()
	{
		$ticket_id = $this->input->get('ticket_id');
		$callback = $this->Tickets->update_status($ticket_id,'4');
		if ($callback["status"] == 200) {
			if ($this->company_id == '') {
				$this->company_id = $this->Tickets->getTicketsByTicketID($ticket_id)["ticket_company"];
			}
			$this->Functions->insertLog($this->tabel,$this->company_id,'working','กำลังดำเนินการ',$this->uid,$ticket_id,'','','ticket');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function assign_working_post()
	{
		$work_id = $this->input->post('work_id');
		$ticket_id = $this->input->post('ticket_id');
		$work_type = $this->input->post('work_type');
		$work_assign_to = $this->input->post('work_assign_to');
		$work_finish = $this->input->post('work_finish');
		$callback = $this->Tickets->create_working($work_id,$ticket_id,$work_type,$this->uid,$work_assign_to,$work_finish);
		if ($callback["status"] == 200) {
			$this->Functions->insertLog($this->tabel,$this->company_id,'assign','มอบหมายงาน',$this->uid,$ticket_id,'',$callback['id'],'ticket_working');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function working_success_get()
	{
		$ticket_id = $this->input->get('ticket_id');
		$callback = $this->Tickets->update_status($ticket_id,'5');
		if ($callback["status"] == 200) {
			if ($this->company_id == '') {
				$this->company_id = $this->Tickets->getTicketsByTicketID($ticket_id)["ticket_company"];
			}
			$this->Functions->insertLog($this->tabel,$this->company_id,'checking','รอตรวจสอบ',$this->uid,$ticket_id,'','','ticket');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function Ticket_success_get()
	{
		$ticket_id = $this->input->get('ticket_id');
		$word_id = $this->Tickets->getWorkIdByTicket($ticket_id);
		$callback = $this->Tickets->working_QC_success($ticket_id,$this->uid);
		if ($callback["status"] == 200) {
			if ($this->company_id == '') {
				$this->company_id = $this->Tickets->getTicketsByTicketID($ticket_id)["ticket_company"];
			}
			$this->Functions->insertLog($this->tabel,$this->company_id,'success','ดำเนินการเสร็จสิ้น',$this->uid,$ticket_id,'',$word_id,'ticket');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function Ticket_eject_get()
	{
		$ticket_id = $this->input->get('ticket_id');
		$reason = $this->input->get('reason');
		$work_id = $this->Tickets->getWorkIdByTicket($ticket_id);
		$callback_qc = $this->Tickets->working_QC_eject($ticket_id,$this->uid,$work_id);
		if ($callback_qc["status"] == 200) {
			if ($this->company_id == '') {
				$this->company_id = $this->Tickets->getTicketsByTicketID($ticket_id)["ticket_company"];
			}
			$log_id = $this->Functions->insertLog($this->tabel,$this->company_id,'eject','ไม่ผ่านการตรวจสอบ',$this->uid,$ticket_id,'',$work_id,'ticket');
			$callback = $this->Tickets->Ticket_Eject($ticket_id,$log_id,$reason);
			if ($callback["status"] == 200) {
				$callback = $this->Tickets->update_status($ticket_id,'1');
				$this->Functions->insertLog($this->tabel,$this->company_id,'reTicket','ส่งคำขออีกครั้ง',$this->uid,$ticket_id,'','','ticket');
			}
		}
		$this->output->set_output(json_encode($callback));
	}

	public function Ticket_ReTicket_get()
	{
		$ticket_id = $this->input->get('ticket_id');
		$callback = $this->Tickets->update_status($ticket_id,'1');
		if ($callback["status"] == 200) {
			if ($this->company_id == '') {
				$this->company_id = $this->Tickets->getTicketsByTicketID($ticket_id)["ticket_company"];
			}
			$this->Functions->insertLog($this->tabel,$this->company_id,'reTicket','ส่งคำขออีกครั้ง',$this->uid,$ticket_id,'','','ticket');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function Ticket_Detail_post()
	{
		$ticket_id = $this->input->post('ticket_id');
		$work_id = $this->input->post('work_id');
		$detail_description = $this->input->post('detail_description');
		$callback = $this->Tickets->create_detail($ticket_id,$work_id,$detail_description,$this->uid);
		if ($callback["status"] == 200) {
			if ($this->company_id == '') {
				$this->company_id = $this->Tickets->getTicketsByTicketID($ticket_id)["ticket_company"];
			}
			$this->Functions->insertLog($this->tabel,$this->company_id,'detail','บันทึกการดำเนินงาน',$this->uid,$ticket_id,'','','ticket');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function Detail_files_post()
	{
		$file = $_FILES['file'];
		$ticket_id = $_POST['id'];
		$detail_id = $_POST['detail_id'];
		$callback = $this->Tickets->file_Detail($ticket_id,$detail_id,$file);
		$this->output->set_output(json_encode($callback));
	}

	public function TicketTracking_get()
	{
		$ticket_id = $this->input->get('ticket_id');
		$callback = $this->Tickets->getTicketTracking($ticket_id);
		$this->output->set_output(json_encode($callback));
	}

	public function TicketDownload_post()
	{
		$ticket_id = $this->input->post('ticket_id');
			if ($this->company_id == '') {
				$this->company_id = $this->Tickets->getTicketsByTicketID($ticket_id)["ticket_company"];
			}
		$this->Functions->insertLog($this->tabel,$this->company_id,'download','บันทึกไฟล์แนบ',$this->uid,$ticket_id,'','','ticket');
	}

	public function TicketDetailWork_get()
	{
		$ticket_id = $this->input->get('ticket_id');
		$callback = $this->Tickets->getTicketDetailWork($ticket_id);
		$this->output->set_output(json_encode($callback));
	}

	public function TicketWorkingSuccess_get()
	{
		$ticket_id = $this->input->get('ticket_id');
		$word_id = $this->Tickets->getWorkIdByTicket($ticket_id);
		$callback = $this->Tickets->update_status($ticket_id,'5');
		if ($callback["status"] == 200) {
			if ($this->company_id == '') {
				$this->company_id = $this->Tickets->getTicketsByTicketID($ticket_id)["ticket_company"];
			}
			$this->Functions->insertLog($this->tabel,$this->company_id,'working success','ให้บริการเสร็จสิ้น',$this->uid,$ticket_id,'',$word_id,'ticket');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function Ticket_edit_post()
	{
		$ticket_id = $this->input->post('ticket_id');
		$ticket_title = $this->input->post('ticket_title');
		$ticket_detail = $this->input->post('ticket_detail');
		$callback = $this->Tickets->edit($ticket_id,$ticket_title,$ticket_detail,$this->uid);
		if ($callback["status"] == 200) {
			$this->Functions->insertLog($this->tabel,$this->company_id,'edit','แก้ไขคำขอ',$this->uid,$ticket_id,'','','ticket');
		}
		$this->output->set_output(json_encode($callback));
	}

}
