<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;
use \Firebase\JWT\JWT;

class Master_controller extends REST_Controller {

	public function __construct(){
		parent::__construct();
		if($_SERVER["REQUEST_METHOD"] != "OPTIONS"){
            $callback["options"] = false;
            $token = $_SERVER["HTTP_TOKEN"];
            $user = $this->Token->decode($token);
						$this->company_id = $user->company_id;
						$this->uid = $user->uid;
						$this->branch_id = $user->branch_id;
						$this->dep_id = $user->dep_id;
						$this->site_id = $user->site_id;
						$this->tabel = 'log_master';
        }else{
            exit;
        }
	}

	// level
	public function create_level_post()
	{
		$name_TH = $this->input->post('thai_name');
		$name_EN = $this->input->post('eng_name');
		$ticket = $this->input->post('ticket');
		$assign = $this->input->post('assign');
		$qc = $this->input->post('qc');
		$admin = $this->input->post('admin');
		$config = $this->input->post('config');
		$callback = $this->MS_levels->create($name_TH,$name_EN,$ticket,$assign,$qc,$admin,$config,$this->company_id);
		if ($callback['status'] == 200) {
		$this->Functions->insertLog($this->tabel,$this->company_id ,'create','สร้างสิทธิ์การใช้งาน',$this->uid,$callback['id'],'','','ms_level');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function edit_level_post()
	{
		$Id = $this->input->post('id');
		$name_TH = $this->input->post('thai_name');
		$name_EN = $this->input->post('eng_name');
		$ticket = $this->input->post('ticket');
		$assign = $this->input->post('assign');
		$qc = $this->input->post('qc');
		$admin = $this->input->post('admin');
		$config = $this->input->post('config');
		$callback = $this->MS_levels->edit($Id,$name_TH,$name_EN,$ticket,$assign,$qc,$admin,$config);
		if ($callback['status'] == 200) {
			$this->Functions->insertLog($this->tabel,$this->company_id ,'edit','แก้ไขสิทธิ์การใช้งาน',$this->uid,$Id,$callback['data']['level_name_th']."(".$callback['data']['level_name_en'].")",$name_TH."(".$name_EN.")",'ms_level');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function data_level_get()
	{
		$company_id = $this->input->get('company_id');
		if ($company_id == "") {
				$company_id = $this->company_id;
		}
		$callback = $this->MS_levels->getDataByRoot($company_id);
		$this->output->set_output(json_encode($callback));
	}

	// branch
	public function create_branch_post()
	{
		$branch_name = $this->input->post('branch_name');
		$branch_caption = $this->input->post('branch_caption');
		$company_id = $this->input->post('company_id');
		if ($company_id == "") {
				$company_id = $this->company_id;
		}
		$callback = $this->MS_branch->create($branch_name,$branch_caption,$company_id);
		if ($callback['status'] == 200) {
		 $this->Functions->insertLog($this->tabel,$this->company_id ,'create','สร้างสาขา',$this->uid,$callback['id'],'','','ms_branch');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function edit_branch_post()
	{
		$branch_id = $this->input->post('branch_id');
		$branch_name = $this->input->post('branch_name');
		$branch_caption = $this->input->post('branch_caption');
		$callback = $this->MS_branch->edit($branch_id,$branch_name,$branch_caption);
		if ($callback['status'] == 200) {
		 	$this->Functions->insertLog($this->tabel,$this->company_id ,'edit','แก้ไขสาขา',$this->uid,$branch_id,$callback['data']['branch_caption']."(".$callback['data']['branch_name'].")",$branch_caption."(".$branch_name.")",'ms_level');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function drop_branch_post()
	{
		$branch_id = $this->input->post('branch_id');
		$callback = $this->MS_branch->change_status($branch_id,'0');
		if ($callback['status'] == 200) {
			$this->Functions->insertLog($this->tabel,$this->company_id ,'delete','ลบสาขา',$this->uid,$Id,'','','ms_level');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function status_branch_post()
	{
		$branch_id = $this->input->post('branch_id');
		$branch_status = $this->input->post('branch_status');
		$callback = $this->MS_branch->update_status($branch_id,$branch_status);
		$this->output->set_output(json_encode($callback));
	}

	public function data_branch_get()
	{
		$company_id = $this->input->get('company_id');
		if ($company_id == "") {
				$company_id = $this->company_id;
		}
		$callback = $this->MS_branch->getDataByRoot($company_id);
		$this->output->set_output(json_encode($callback));
	}

	// department
	public function create_department_post()
	{
		$dep_name = $this->input->post('dep_name');
		$dep_caption = $this->input->post('dep_caption');
		$branch_id = $this->input->post('branch_id');
		$callback = $this->MS_department->create($dep_name,$dep_caption,$branch_id);
		if ($callback['status'] == 200) {
		 $this->Functions->insertLog($this->tabel,$this->company_id ,'create','สร้างแผนก',$this->uid,$callback['id'],'','','ms_department');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function edit_department_post()
	{
		$dep_id = $this->input->post('dep_id');
		$dep_name = $this->input->post('dep_name');
		$dep_caption = $this->input->post('dep_caption');
		$branch_id = $this->input->post('branch_id');
		$callback = $this->MS_department->edit($dep_id,$dep_name,$dep_caption,$branch_id);
		if ($callback['status'] == 200) {
			$this->Functions->insertLog($this->tabel,$this->company_id ,'edit','แก้ไขแผนก',$this->uid,$dep_id,
			$callback['data']['dep_caption']."(".$callback['data']['dep_name'].") ของสาขา ".$callback['data']['branch_id'],
			$dep_caption."(".$dep_name.") ของสาขา ".$branch_id,'ms_level');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function drop_department_post()
	{
		$dep_id = $this->input->post('dep_id');
		$callback = $this->MS_department->change_status($dep_id,'0');
		if ($callback['status'] == 200) {
			$this->Functions->insertLog($this->tabel,$this->company_id ,'delete','ลบแผนก',$this->uid,$dep_id,'','','ms_department');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function status_department_post()
	{
		$dep_id = $this->input->post('dep_id');
		$dep_status = $this->input->post('dep_status');
		$callback = $this->MS_department->update_status($dep_id,$dep_status);
		$this->output->set_output(json_encode($callback));
	}

	public function data_department_get()
	{
		$branch_id = $this->input->get('branch_id');
		if ($branch_id == "") {
				$branch_id = $this->branch_id;
		}
		$callback = $this->MS_department->getDataByRoot($branch_id);
		$this->output->set_output(json_encode($callback));
	}

	// site
	public function create_site_post()
	{
		$site_name = $this->input->post('site_name');
		$site_description = $this->input->post('site_description');
		$dep_id = $this->input->post('dep_id');
		$callback = $this->MS_site->create($site_name,$site_description,$dep_id);
		if ($callback['status'] == 200) {
		 	 $this->Functions->insertLog($this->tabel,$this->company_id ,'create','สร้างไซต์',$this->uid,$callback['id'],'','','ms_site');
			}
		$this->output->set_output(json_encode($callback));
	}

	public function edit_site_post()
	{
		$site_id = $this->input->post('site_id');
		$site_name = $this->input->post('site_name');
		$site_description = $this->input->post('site_description');
		$dep_id = $this->input->post('dep_id');
		$callback = $this->MS_site->edit($site_id,$site_name,$site_description,$dep_id);
		if ($callback['status'] == 200) {
			$this->Functions->insertLog($this->tabel,$this->company_id ,'edit','แก้ไขไซต์',$this->uid,$site_id,
			$callback['data']['site_name']."(".$callback['data']['site_description'].") ของแผนก ".$callback['data']['dep_id'],
			$site_name."(".$site_description.") ของแผนก ".$dep_id,'ms_site');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function drop_site_post()
	{
		$site_id = $this->input->post('site_id');
		$callback = $this->MS_site->change_status($site_id,'0');
		if ($callback['status'] == 200) {
			$this->Functions->insertLog($this->tabel,$this->company_id ,'delete','ลบแผนก',$this->uid,$site_id,'','','ms_department');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function status_site_post()
	{
		$site_id = $this->input->post('site_id');
		$site_status = $this->input->post('site_status');
		$callback = $this->MS_site->update_status($site_id,$site_status);
		$this->output->set_output(json_encode($callback));
	}

	public function data_site_get()
	{
		$dep_id = $this->input->get('dep_id');
		if ($dep_id == "") {
				$dep_id = $this->dep_id;
		}
		$callback = $this->MS_site->getDataByRoot($dep_id);
		$this->output->set_output(json_encode($callback));
	}

	// team
	public function create_team_post()
	{
		$team_name = $this->input->post('team_name');
		$site_id = $this->input->post('site_id');
		$callback = $this->MS_team->create($team_name,$site_id);
		if ($callback['status'] == 200) {
		 $this->Functions->insertLog($this->tabel,$this->company_id ,'create','สร้างทีม',$this->uid,$callback['id'],'','','ms_team');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function edit_team_post()
	{
		$team_id = $this->input->post('team_id');
		$team_name = $this->input->post('team_name');
		$site_id = $this->input->post('site_id');
		$callback = $this->MS_team->edit($team_id,$team_name,$site_id);
		if ($callback['status'] == 200) {
			$this->Functions->insertLog($this->tabel,$this->company_id ,'edit','แก้ไขทีม',$this->uid,$team_id,
			$callback['data']['team_name']." ของไซต์ ".$callback['data']['site_id'],
			$team_name." ของแผนก ".$site_id,'ms_team');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function drop_team_post()
	{
		$team_id = $this->input->post('team_id');
		$callback = $this->MS_team->change_status($team_id,'0');
		if ($callback['status'] == 200) {
			$this->Functions->insertLog($this->tabel,$this->company_id ,'delete','ลบทีม',$this->uid,$team_id,'','','ms_team');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function status_team_post()
	{
		$team_id = $this->input->post('team_id');
		$team_status = $this->input->post('team_status');
		$callback = $this->MS_team->update_status($team_id,$team_status);
		$this->output->set_output(json_encode($callback));
	}

	public function data_team_get()
	{
		$site_id = $this->input->get('site_id');
		if ($site_id == "") {
				$site_id = $this->site_id;
		}
		$callback = $this->MS_team->getDataByRoot($site_id);
		$this->output->set_output(json_encode($callback));
	}

	public function dataBysite_team_get()
	{
		$site_id = $this->input->get('site_id');
		$callback = $this->MS_team->getDataByCompanyId($site_id);
		$this->output->set_output(json_encode($callback));
	}

}
