<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Users_controller extends REST_Controller {

	public function __construct(){
		parent::__construct();
		if($_SERVER["REQUEST_METHOD"] != "OPTIONS"){
            $callback["options"] = false;
            $token = $_SERVER["HTTP_TOKEN"];
            $user = $this->Token->decode($token);
						$this->company_id = $user->company_id;
						$this->uid = $user->uid;
						$this->branch_id = $user->branch_id;
						$this->dep_id = $user->dep_id;
						$this->site_id = $user->site_id;
						$this->team_id = $user->team_id;
						$this->operator = $user->operator;
						if ($this->operator == 1) {
							$this->user_oper = $user->user_oper->data;
						}
						$this->tabel = 'log_users';
						$this->load->model('Email');
						$this->load->model('Functions');
        }else{
            exit;
        }
	}

	public function create_user_post()
	{
		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$fname = $this->input->post('fname');
		$lname = $this->input->post('lname');
		$gender = $this->input->post('gender');
		$company_id = $this->input->post('company_id');
		$branch_id = $this->input->post('branch_id');
		$dep_id = $this->input->post('dep_id');
		$site_id = $this->input->post('site_id');
		$team_id = $this->input->post('team_id');
		$level_id = $this->input->post('level_id');
		$operator = 1;
		// $operator = $this->input->post('operator');
		$chkuser= $this->Users->chk_duplicate_username($username);
		if ($chkuser['status'] == 200) {
			$callback = $this->Users->createuser($username,$email,$password,$fname,$lname,$gender,
			$company_id,$branch_id,$dep_id,$site_id,$team_id,$level_id,$this->uid,$operator);
			if ($callback["status"] == 200) {
				$this->Email->SendMailConfirm($callback["id"]);
				$this->Functions->insertLog($this->tabel,$this->company_id,'create','สร้างผู้ใช้',$this->uid,$callback["id"],'','','users');
			}
			$this->output->set_output(json_encode($callback));
		}else {
			$this->output->set_output(json_encode($chkuser));
		}
	}

	public function create_operator_post()
	{
		$uid = $this->input->post('uid');
		$company_id = $this->input->post('company_id');
		$branch_id = $this->input->post('branch_id');
		$dep_id = $this->input->post('dep_id');
		$site_id = $this->input->post('site_id');
		$team_id = $this->input->post('team_id');
		$level_id = $this->input->post('level_id');
		$callback = $this->Users_operator->createoperator($uid,$company_id,$branch_id,$dep_id,$site_id,$team_id,$this->uid,$level_id);
		if ($callback["status"] == 200) {
			$this->Functions->insertLog($this->tabel,$this->company_id,'create','สร้างผู้ประสานงาน',$this->uid,$callback["id"],'','','users_operator');
		}
		$this->output->set_output(json_encode($callback));
	}

	public function Profile_get()
	{
		$callback = $this->Users->getProfile($this->uid);
		$this->output->set_output(json_encode($callback));
	}

	public function ProfileDetail_get()
	{
		$Id = $this->input->post('uid');
		if ($Id == '') {
			$Id = $this->uid;
		}
		$callback = $this->Users->getProfileDetail($Id);
		$this->output->set_output(json_encode($callback));
	}

	public function userCompany_get()
	{
		$status = $this->input->get('status');
		$company_id = $this->input->get('company_id');
		if ($company_id == '') {
			$company_id = $this->company_id;
		}
		$branch_id = $this->input->get('branch_id');
		if ($branch_id == '') {
			$company = $this->branch_id;
		}
		$dep_id = $this->input->get('dep_id');
		if ($dep_id == '') {
			$company = $this->dep_id;
		}
		$site_id = $this->input->get('site_id');
		if ($site_id == '') {
			$company = $this->site_id;
		}
		$callback = $this->Users->getAlluser($company_id,$branch_id,$dep_id,$site_id,$team_id,$level_id);
		$this->output->set_output(json_encode($callback));
	}

	public function UserService_get()
	{
		$company_id = $this->input->get('company_id');
		$branch_id = $this->input->get('branch_id');
		$dep_id = $this->input->get('dep_id');
		$site_id = $this->input->get('site_id');
		$team_id = $this->input->get('team_id');
		$callback = $this->Users->getUserService($company_id,$branch_id,$dep_id,$site_id,$team_id);
		$this->output->set_output(json_encode($callback));
	}
}
