<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;
use \Firebase\JWT\JWT;

class Company_controller extends REST_Controller {

	public function __construct(){
		parent::__construct();
		if($_SERVER["REQUEST_METHOD"] != "OPTIONS"){
            $callback["options"] = false;
            $token = $_SERVER["HTTP_TOKEN"];
            $user = $this->Token->decode($token);
						$this->company_id = $user->company_id;
						$this->uid = $user->uid;
						$this->branch_id = $user->branch_id;
						$this->dep_id = $user->dep_id;
						$this->site_id = $user->site_id;
						$this->tabel = 'log_master';
        }else{
            exit;
        }
	}

	public function create_post()
	{
		$name_TH = $this->input->post('thai_name');
		$name_EN = $this->input->post('eng_name');
		$Chkname_TH =  $this->Company->chk_duplicate_name_TH($name_TH);
		$Chkname_EN =  $this->Company->chk_duplicate_name_EN($name_EN);
		if ($Chkname_TH['status'] == 200 && $Chkname_EN['status'] == 200 ){
			$callback = $this->Company->create($name_TH,$name_EN);
			if ($callback['status'] == 200){
				$this->Functions->insertLog($this->tabel,$callback['id'],'create','สร้างบริษัท',$this->uid,$callback['id'],'','','company');
			}
			$this->output->set_output(json_encode($callback));
		}elseif ($Chkname_TH['status'] == 200 && $Chkname_EN['status'] == 401){
			$this->output->set_output(json_encode($Chkname_EN));
		}elseif ($Chkname_TH['status'] == 401 && $Chkname_EN['status'] == 200){
			$this->output->set_output(json_encode($Chkname_TH));
		}else{
			$callback = array(
                          "status" => 401,
                          "type" => FALSE,
                          "msg" => "Duplicate",
                        );
			$this->output->set_output(json_encode($callback));
		}
	}

	public function getDatacompany_get()
	{
		$status = $this->input->post('status');
		$callback = $this->Company->getCompany($status);
		$this->output->set_output(json_encode($callback));
	}
}
